package memq

import (
	"encoding/json"
	"fmt"

	"github.com/Synthesis-AI-Dev/broker"
)

// Memory implements the broker.MessageBroker interface to provide message
// publish and retrieve functionality based on an in-memory queue.
//
// Not thread safe.
type MemQ struct {
	queues map[string]*Queue
}

// Queue implements the broker.Queue interface.
type Queue struct {
	name       string
	memq       *MemQ
	definition *broker.QueueDefinition
	msgs       []Message
}

// Message is what is stored on the Queue.
type Message struct {
	ID      string
	payload []byte
}

// MessageJSON implements the broker.ReceiveMessage interface.
type MessageJSON struct {
	memq          *MemQ
	queue         *Queue
	receiptHandle string
	payload       []byte
}

// ID returns the identifier created by SQS that represents this message
// on the broker.
func (m *MessageJSON) ID() string {
	return m.receiptHandle
}

// New creates the Broker
func New() *MemQ {
	return &MemQ{
		queues: make(map[string]*Queue),
	}
}

func (m *MemQ) createQueue(qd *broker.QueueDefinition) *Queue {
	return &Queue{
		name:       qd.CanonicalName,
		memq:       m,
		definition: qd,
	}
}

// CreateQueue returns the queue and its dlq if they exist or creates
// them if they do not.
func (m *MemQ) CreateQueue(qd *broker.QueueDefinition) ([]broker.Queue, error) {
	var dlq *Queue

	if qd.DLQ != nil {
		dlq := m.queues[qd.DLQ.CanonicalName]
		if dlq == nil {
			dlq = m.createQueue(qd.DLQ)
		}
	}

	q := m.queues[qd.CanonicalName]
	if q == nil {
		q = m.createQueue(qd)
	}

	queues := make([]broker.Queue, 0, 2)
	queues = append(queues, q)
	if dlq != nil {
		queues = append(queues, dlq)
	}
	return queues, nil
}

// SendOne converts sends a message to the queue.
func (q *Queue) SendOne(m broker.SendMessage) error {
	marshal, err := m.Marshal()
	if err != nil {
		return err
	}

	q.msgs = append(q.msgs, Message{
		payload: marshal,
	})
	return nil
}

func (q *Queue) SendMany(msgs []broker.SendMessage) error {
	if len(msgs) > 10 {
		return fmt.Errorf("Cannot exceed 10 messages")
	}
	if len(msgs) == 0 {
		return fmt.Errorf("Empty slice error")
	}
	mMsgs := make([][]byte, 0, len(msgs))
	for _, m := range msgs {
		marshal, err := m.Marshal()
		if err != nil {
			return fmt.Errorf("memq.SendMany: error marshaling msg: %v", err)
		}
		mMsgs = append(mMsgs, marshal)
	}

	for _, m := range mMsgs {
		q.msgs = append(q.msgs, Message{
			payload: m,
		})
	}
	return nil
}

// ReceiveOne fetches a mesage from the message broker. If there are no
// messages on the queue an error (broker.ErrQueueNoMessages) is returned.
//
// After handling the message, make sure to Ack() to remove the message from
// the queue or it will be redelivered after its VisibilityTimeout.
func (q *Queue) ReceiveOne() (broker.ReceiveMessage, error) {
	if len(q.msgs) == 0 {
		return nil, broker.ErrQueueNoMessages
	}
	msg := q.msgs[0]
	q.msgs = q.msgs[1:]
	return &MessageJSON{
		memq:          q.memq,
		queue:         q,
		payload:       msg.payload,
		receiptHandle: "foo",
	}, nil

}

// FetchAttributes makes a request to SQS to retreive the attributes specified.
func (q *Queue) FetchAttributes(attrs []string) (map[string]string, error) {
	return nil, nil
}

func (q *Queue) String() string {
	return q.name
}

// Delete removes the queue.
func (q *Queue) Delete() error {
	delete(q.memq.queues, q.name)
	return nil
}

// MoveMessages is a stub that does nothing.
func (q *Queue) MoveMessages(dest broker.Queue) (int, error) {
	return 0, nil
}

// SetVisibilityTimeout is a stub that does nothing.
func (m *MessageJSON) SetVisibilityTimeout(t int) error {
	return nil
}

// Ack is a stub that does nothing.
func (m *MessageJSON) Ack() error {
	return nil
}

// Unmarshal implements the broker.ReceiveMessage interface and returns the
// contents received from the broker into v.
func (m *MessageJSON) Unmarshal(v interface{}) error {
	return json.Unmarshal(m.payload, v)
}
