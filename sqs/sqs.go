package sqs

import (
	"bytes"
	"compress/zlib"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"strconv"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"

	"github.com/Synthesis-AI-Dev/broker"
	"github.com/Synthesis-AI-Dev/broker/lib/generic"
)

var stopReceiveSleepDuration = 250 * time.Millisecond

const gzip = "gzip"
const msgStoredInAttributes = "__see_attributes__"

// SQS implements the broker.MessageBroker interface to provide message
// publish and retrieve functionality provided by AWS SQS.
type SQS struct {
	client *sqs.SQS
	// number of seconds between receive message calls
	waitTimeSeconds int64
}

// Queue implements the broker.Queue interface.
type Queue struct {
	url           string
	name          string
	sqs           *SQS
	inStopReceive int32
	receiving     int32
	definition    *broker.QueueDefinition
}

// MessageJSON implements the broker.ReceiveMessage interface.
type MessageJSON struct {
	sqs           *SQS
	queue         *Queue
	receiptHandle *string
	payload       []byte
}

// ID returns the identifier created by SQS that represents this message
// on the broker.
func (m *MessageJSON) ID() string {
	return *m.receiptHandle
}

// New creates an sqs client to interact with aws sqs using the provided
// configuration.
func New(config aws.Config, waitTimeSeconds int) *SQS {
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		Config:            config,
		SharedConfigState: session.SharedConfigEnable,
	}))

	return &SQS{
		waitTimeSeconds: int64(waitTimeSeconds),
		client:          sqs.New(sess),
	}
}

func (s *SQS) getQueueURL(name string) (string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 200*time.Millisecond)
	defer cancel()
	res, err := s.client.GetQueueUrlWithContext(ctx, &sqs.GetQueueUrlInput{
		QueueName: &name,
	})
	if err != nil {
		return "", err
	}
	return *res.QueueUrl, nil
}

func (s *SQS) createQueue(qd *broker.QueueDefinition, dlqArn string) (*Queue, error) {
	attrs := make(map[string]*string)
	for k, v := range qd.Options {
		attrs[k] = &v
	}
	name := qd.CanonicalName

	var res *sqs.CreateQueueOutput
	err := generic.Retry(4, 2*time.Second, func() error {
		if dlqArn != "" {
			qd.Policy["deadLetterTargetArn"] = dlqArn
			redrivePolicy, err := json.Marshal(qd.Policy)
			if err != nil {
				log.WithFields(log.Fields{
					"queue": name,
				}).Fatal("no policy specified for dlq enabled queue")
			}
			attrs["RedrivePolicy"] = aws.String(string(redrivePolicy))
		}

		ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
		defer cancel()
		var err error
		if len(attrs) == 0 {
			// aws doesn't like it when you give it an empty map
			attrs = nil
		}
		res, err = s.client.CreateQueueWithContext(ctx, &sqs.CreateQueueInput{
			QueueName:  &name,
			Attributes: attrs,
		})
		if err != nil {
			log.WithFields(log.Fields{
				"err":   err,
				"queue": name,
				"attrs": attrs,
			}).Warn("error creating queue")
		}
		return err
	})
	if err != nil {
		return nil, err
	}

	return s.newQueue(res.QueueUrl, qd), nil
}

func (s *SQS) newQueue(URL *string, qd *broker.QueueDefinition) *Queue {
	return &Queue{
		url:        *URL,
		sqs:        s,
		name:       qd.CanonicalName,
		definition: qd,
	}
}

// CreateQueue creates the named queue if it does not exist, otherwise it
// creates the queue with the options.
func (s *SQS) CreateQueue(qd *broker.QueueDefinition) ([]broker.Queue, error) {
	var dlq *Queue
	var dlqArn string

	if qd.DLQ != nil {
		ctx, cancel := context.WithTimeout(context.Background(), 800*time.Millisecond)
		defer cancel()

		// does the dead letter queue exist or do we need to create it too?
		dlqRes, err := s.client.GetQueueUrlWithContext(ctx, &sqs.GetQueueUrlInput{
			QueueName: &qd.DLQ.CanonicalName,
		})
		if err != nil {
			// dlq does not exist, create it
			queues, err := s.CreateQueue(qd.DLQ)
			if err != nil {
				log.WithFields(log.Fields{
					"err":   err,
					"dlq":   qd.DLQ.CanonicalName,
					"queue": qd.CanonicalName,
				}).Error("failed to create dlq")
				return nil, err
			}
			dlq, _ = queues[0].(*Queue)
			// to make the new queue into a dead-letter queue, we
			// configure another queue with the dlqarn in its
			// redrive policy. Grab the queuearn so we can do
			// that..
			attrs, err := dlq.FetchAttributes([]string{"QueueArn"})
			if err != nil {
				log.WithFields(log.Fields{
					"err": err,
					"dlq": qd.DLQ.CanonicalName,
				}).Error("failed to fetch dlq arn ")
				return nil, err
			}
			dlqArn = attrs["QueueArn"]
		} else {
			dlq = s.newQueue(dlqRes.QueueUrl, qd.DLQ)
		}
	}

	ctx, cancel := context.WithTimeout(context.Background(), 800*time.Millisecond)
	defer cancel()
	res, err := s.client.GetQueueUrlWithContext(ctx, &sqs.GetQueueUrlInput{
		QueueName: &qd.CanonicalName,
	})

	var q *Queue
	if err != nil {
		q, err = s.createQueue(qd, dlqArn)
		if err != nil {
			log.WithFields(log.Fields{
				"err":   err,
				"queue": qd.CanonicalName,
			}).Error("failed to create queue")
			return nil, err
		}
	} else {
		q = s.newQueue(res.QueueUrl, qd)
	}

	queues := make([]broker.Queue, 0, 2)
	queues = append(queues, q)
	if dlq != nil {
		queues = append(queues, dlq)
	}
	return queues, nil
}

func newGzipAttribute(body []byte) *sqs.MessageAttributeValue {
	var buff bytes.Buffer
	w := zlib.NewWriter(&buff)
	w.Write(body)
	w.Close()
	return &sqs.MessageAttributeValue{
		BinaryValue: buff.Bytes(),
		DataType:    aws.String("Binary.gzip"),
	}
}

// SendOne converts sends a message to the queue.
func (q *Queue) SendOne(m broker.SendMessage) error {
	return generic.Retry(4, 2*time.Second, func() error {
		marshal, err := m.Marshal()
		if err != nil {
			return err
		}
		body := string(marshal)

		mi := sqs.SendMessageInput{
			QueueUrl:    &q.url,
			MessageBody: &body,
		}

		if q.definition.UseGzip {
			attrs := make(map[string]*sqs.MessageAttributeValue)
			attrs[gzip] = newGzipAttribute(marshal)
			mi.SetMessageAttributes(attrs)
			mi.SetMessageBody(msgStoredInAttributes)
		}

		_, err = q.sqs.client.SendMessage(&mi)
		if err != nil {
			if aerr, ok := err.(awserr.Error); ok {
				switch aerr.Code() {
				case sqs.ErrCodeInvalidMessageContents,
					sqs.ErrCodeUnsupportedOperation:
					return generic.NewStopErr(err)
				default:
					log.WithFields(log.Fields{
						"err":   err,
						"queue": q.name,
					}).Warn("error sending message")
					return err
				}
			}
		}
		return err
	})
}

func convertToBatchRequestEntries(msgs [][]byte, useGzip bool) []*sqs.SendMessageBatchRequestEntry {
	reqs := make([]*sqs.SendMessageBatchRequestEntry, 0, len(msgs))
	for i, msg := range msgs {
		req := &sqs.SendMessageBatchRequestEntry{
			Id:          aws.String(strconv.Itoa(i)),
			MessageBody: aws.String(string(msg)),
		}
		if useGzip {
			attrs := make(map[string]*sqs.MessageAttributeValue)
			attrs[gzip] = newGzipAttribute(msg)
			req.SetMessageAttributes(attrs)
			req.SetMessageBody(msgStoredInAttributes)

		}
		reqs = append(reqs, req)
	}
	return reqs
}

// Constructs a string with the error message from aws.
func convertBatchErrors(errs []*sqs.BatchResultErrorEntry) error {
	var errMsgs []string
	for _, err := range errs {
		errMsgs = append(errMsgs, fmt.Sprintf("%s:%s", *err.Id, *err.Message))
	}
	return errors.New(strings.Join(errMsgs, ", "))
}

// SendMany sends up to 10 messages to the SQS queue. An error is returned if
// an error is returned for any message sent to the queue.
//
// An error returned from this method does not indicate that all the messages
// failed to be sent to SQS, but that *at least* one failed to send.
func (q *Queue) SendMany(msgs []broker.SendMessage) error {
	if len(msgs) > 10 {
		return fmt.Errorf("sqs.SendMany: cannot exceed 10 messages")
	}
	if len(msgs) == 0 {
		return fmt.Errorf("sqs.SendMany: empty slice error")
	}
	mMsgs := make([][]byte, 0, 10)
	for _, m := range msgs {
		marshal, err := m.Marshal()
		if err != nil {
			return fmt.Errorf("sqs.SendMany: error marshaling msg: %v", err)
		}
		mMsgs = append(mMsgs, marshal)
	}

	batchReq := convertToBatchRequestEntries(mMsgs, q.definition.UseGzip)
	return generic.Retry(4, 2*time.Second, func() error {
		input := sqs.SendMessageBatchInput{
			QueueUrl: &q.url,
			Entries:  batchReq,
		}
		output, err := q.sqs.client.SendMessageBatch(&input)
		if _, ok := err.(awserr.Error); ok {
			log.WithFields(log.Fields{
				"queue": q.name,
				"err":   err,
			}).Error("sqs send message batch error")
			return generic.NewStopErr(err)
		}
		if len(output.Failed) > 0 {
			// unclear if the failures could be retried here as the
			// error codes are not documented, even so..this is
			// easiest for now.
			err := convertBatchErrors(output.Failed)
			return generic.NewStopErr(err)
		}
		return err
	})
}

// ReceiveOne fetches a mesage from the message broker if there is one
// on the queue, otherwise it returns broker.ErrQueueNoMessages
//
// After handling the message, make sure to Ack() to remove the message from
// the queue or it will be redelivered after its VisibilityTimeout.
func (q *Queue) ReceiveOne() (broker.ReceiveMessage, error) {
	res, err := q.sqs.client.ReceiveMessage(&sqs.ReceiveMessageInput{
		QueueUrl:        &q.url,
		WaitTimeSeconds: aws.Int64(q.sqs.waitTimeSeconds),
		// specifying 'All' so we pick our zlib compressed messages
		MessageAttributeNames: []*string{aws.String("All")},
	})
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case sqs.ErrCodeOverLimit:
				return nil, err
			}
		}
		log.WithFields(log.Fields{
			"queue": q.name,
			"err":   err,
		}).Warn("sqs poll error")

		return nil, broker.ErrQueuePollError
	}
	if len(res.Messages) == 0 {
		return nil, broker.ErrQueueNoMessages
	}

	var payload []byte
	msg := res.Messages[0]
	if *msg.Body == msgStoredInAttributes {
		if val := msg.MessageAttributes[gzip]; val != nil {
			reader := bytes.NewReader(val.BinaryValue)
			r, err := zlib.NewReader(reader)
			if err != nil {
				return nil, err
			}
			buff := bytes.NewBufferString("")
			io.Copy(buff, r)
			payload = buff.Bytes()
		} else {
			return nil, fmt.Errorf("missing gzip MessageAttribute")
		}

	} else {
		payload = []byte(*msg.Body)
	}

	return &MessageJSON{
		sqs:           q.sqs,
		queue:         q,
		payload:       payload,
		receiptHandle: res.Messages[0].ReceiptHandle,
	}, nil
}

// FetchAttributes makes a request to SQS to retreive the attributes specified.
func (q *Queue) FetchAttributes(attrs []string) (map[string]string, error) {
	awsAttrs := make([]*string, 0, len(attrs))
	for _, s := range attrs {
		awsAttrs = append(awsAttrs, aws.String(s))
	}

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()
	output, err := q.sqs.client.GetQueueAttributesWithContext(ctx, &sqs.GetQueueAttributesInput{
		AttributeNames: awsAttrs,
		QueueUrl:       &q.url,
	})
	if err != nil {
		return nil, err
	}

	attrMap := make(map[string]string, len(attrs))
	for _, s := range attrs {
		val, ok := output.Attributes[s]
		if !ok {
			log.WithFields(log.Fields{
				"queue":     q.name,
				"attribute": s,
			}).Warn("attribute not returned")
			return nil, fmt.Errorf("did not receive expected attribute")
		}
		attrMap[s] = *val
	}
	return attrMap, nil
}

func (q *Queue) String() string {
	return q.name
}

// Delete removes the queue from SQS.
func (q *Queue) Delete() error {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	_, err := q.sqs.client.DeleteQueueWithContext(ctx, &sqs.DeleteQueueInput{
		QueueUrl: &q.url,
	})
	if err != nil {
		return err
	}
	return err
}

// MoveMessages moves all messages from this queue to the destination queue.
//
// Probably only for the case where we're moving messages from a dead-letter
// queue back to its source queue.
func (q *Queue) MoveMessages(dest broker.Queue) (int, error) {
	dst, ok := dest.(*Queue)
	if !ok {
		log.Fatalf("can only move messages among sqs queues")
	}

	count := 0
	for {
		maxNumberOfMessages := int64(10)
		visibilityTimeout := int64(20)
		resp, err := q.sqs.client.ReceiveMessage(&sqs.ReceiveMessageInput{
			WaitTimeSeconds:     &q.sqs.waitTimeSeconds,
			MaxNumberOfMessages: &maxNumberOfMessages,
			VisibilityTimeout:   &visibilityTimeout,
			QueueUrl:            &q.url,
			// specifying 'All' so we pick our zlib compressed messages
			MessageAttributeNames: []*string{aws.String("All")},
		})

		if err != nil {
			return count, err
		}

		messages := resp.Messages
		messageCount := len(messages)
		if messageCount == 0 {
			return count, nil
		}

		var sendMessageBatchRequestEntries []*sqs.SendMessageBatchRequestEntry
		for index, element := range messages {
			i := strconv.Itoa(index)
			sendMessageBatchRequestEntries = append(sendMessageBatchRequestEntries,
				&sqs.SendMessageBatchRequestEntry{
					Id:                &i,
					MessageBody:       element.Body,
					MessageAttributes: element.MessageAttributes,
				},
			)
		}

		_, err = q.sqs.client.SendMessageBatch(&sqs.SendMessageBatchInput{
			Entries:  sendMessageBatchRequestEntries,
			QueueUrl: &dst.url,
		})
		if err != nil {
			return count, err
		}

		var deleteMessageBatchRequestEntries []*sqs.DeleteMessageBatchRequestEntry
		for index, element := range messages {
			i := strconv.Itoa(index)
			deleteMessageBatchRequestEntries = append(deleteMessageBatchRequestEntries,
				&sqs.DeleteMessageBatchRequestEntry{
					Id:            &i,
					ReceiptHandle: element.ReceiptHandle,
				},
			)
		}
		_, err = q.sqs.client.DeleteMessageBatch(&sqs.DeleteMessageBatchInput{
			Entries:  deleteMessageBatchRequestEntries,
			QueueUrl: &q.url,
		})
		if err != nil {
			return count, err
		}

		count += messageCount
	}
}

// SetVisibilityTimeout sets the number of seconds that a message should not be visible to
// other consumers.
func (m *MessageJSON) SetVisibilityTimeout(numSeconds int) error {
	return generic.Retry(4, 2*time.Second, func() error {
		timeout := int64(numSeconds)

		_, err := m.sqs.client.ChangeMessageVisibility(&sqs.ChangeMessageVisibilityInput{
			ReceiptHandle:     m.receiptHandle,
			QueueUrl:          &m.queue.url,
			VisibilityTimeout: &timeout,
		})
		if err != nil {
			if aerr, ok := err.(awserr.Error); ok {
				switch aerr.Code() {
				case sqs.ErrCodeMessageNotInflight, sqs.ErrCodeReceiptHandleIsInvalid:
					return generic.NewStopErr(err)
				default:
					log.WithFields(log.Fields{
						"err":     aerr,
						"queue":   m.queue.name,
						"receipt": m.receiptHandle,
					}).Warn("error setting message visibility")
					return err
				}
			}
		}
		return err
	})
}

// Ack acknowledges receipt of the message. This must be performed or the
// message will reappear on the queue after its invisibility timeout.
func (m *MessageJSON) Ack() error {
	return generic.Retry(4, 2*time.Second, func() error {
		ctx, cancel := context.WithTimeout(context.Background(), 500*time.Millisecond)
		defer cancel()

		_, err := m.sqs.client.DeleteMessageWithContext(ctx, &sqs.DeleteMessageInput{
			QueueUrl:      &m.queue.url,
			ReceiptHandle: m.receiptHandle,
		})
		if err != nil {
			if aerr, ok := err.(awserr.Error); ok {
				switch aerr.Code() {
				case sqs.ErrCodeInvalidIdFormat, sqs.ErrCodeReceiptHandleIsInvalid:
					return generic.NewStopErr(err)
				default:
					log.WithFields(log.Fields{
						"err":     aerr,
						"queue":   m.queue.name,
						"receipt": m.receiptHandle,
					}).Warn("error acking")
					return err
				}
			}
		}
		return err
	})
}

// Unmarshal implements the broker.ReceiveMessage interface and returns the
// contents received from the broker into v.
func (m *MessageJSON) Unmarshal(v interface{}) error {
	// TODO: unmarshaller should be configurable from the qdef and probably
	// defined as part of the 'ReceiveMessage'
	return json.Unmarshal(m.payload, v)
}
