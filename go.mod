module github.com/Synthesis-AI-Dev/broker

go 1.13

require (
	github.com/aws/aws-sdk-go v1.29.34
	github.com/prometheus/client_golang v1.7.1
	github.com/sirupsen/logrus v1.5.0
)
