package broker

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	log "github.com/sirupsen/logrus"
)

// QueueName is the name of the queue as used by the developer when
// specifying queue names to process from the command line. They are
// prepended by a string (environemnt or username) when created or
// accessed by MessageBroker implementations.
type QueueName string

// ErrQueueNoMessages indicates that there were no messages in the queue.
var ErrQueueNoMessages = errors.New("no messages")

// ErrQueuePollError indicates a temporary problem during queue polling.
var ErrQueuePollError = errors.New("poll error")

// This is how we refer to the queue in log messages and on the command line.
// The actual name of the queue is a combination of strings, depending on
// environment, etc.
func (qn QueueName) String() string {
	return string(qn)
}

// DefBuckets are the default Histogram buckets. The defaults are tailored
// to measure the consumption time (in seconds) of of a message. Most likely,
// however, you will be required to define buckets for your use case.
var DefBuckets = prometheus.ExponentialBuckets(.5, 2, 12) // upper bound of 5 minutes

// MessageConsumer is an interface for processing messages received from the
// broker.
type MessageConsumer interface {
	// HandleMessage processes the message. If true is returned, the
	// message is acknowledged and removed from the message queue. If false
	// is returned, this message will reappear on the message queue.
	Consume(qm *QueueManager, q Queue, msg ReceiveMessage) bool
}

// QueueDefinition houses all the configuration needed to create a queue.
type QueueDefinition struct {
	// The name of the queue minus the prefix. This is how we refer
	// to the queue in the code and from the command line.
	Name QueueName
	// The name of the queue as it exists on the message broker. This
	// is the name plus a prefix.
	CanonicalName string
	// Queue configuration options
	Options map[string]string
	// Queue (redrive) policy options
	Policy   map[string]string
	consumer MessageConsumer
	// If specified, this is the name of the dead letter queue as it exists
	// on the broker. If set, this queue will be created and configured as
	// a dead letter queue for this queue. In addition, any policy options
	// in Policy will be applied to the queue during queue creation.
	DLQ *QueueDefinition
	// if enabled the contents of the message will be compressed while
	// stored on the queue. Message will still be received uncompressed.
	UseGzip bool

	// Buckets used in the consumption time histogram for this queue. Histogram
	// disabled if not specified during queue creation.
	ConsumptionBuckets []float64
}

// QueueDefinitionOption is a functional option that can modify a QueueDefinition.
// Used to create a more readable constructor.
type QueueDefinitionOption func(*QueueDefinition)

// WithQueueOptions adds broker options to the QueueDefinition.
func WithQueueOptions(opts map[string]string) QueueDefinitionOption {
	return func(qd *QueueDefinition) {
		if qd.Options != nil {
			if policy, ok := qd.Options["Policy"]; ok {
				opts["Policy"] = policy
			}
		}
		qd.Options = opts
	}
}

// WithQueuePolicy adds broker policy options to the QueueDefinition.
func WithQueuePolicy(policy map[string]string) QueueDefinitionOption {
	return func(qd *QueueDefinition) {
		qd.Policy = policy
	}
}

// WithDLQ configures the queue with a dead letter queue.
func WithDLQ(d *QueueDefinition) QueueDefinitionOption {
	return func(qd *QueueDefinition) {
		qd.DLQ = d
	}
}

// WithGzip enables transparent message compression and decompression.
func WithGzip() QueueDefinitionOption {
	return func(qd *QueueDefinition) {
		qd.UseGzip = true
	}
}

// WithConsumptionBuckets configures the buckets used in the Histogram that
// tracks message consumption time.
func WithConsumptionBuckets(buckets []float64) QueueDefinitionOption {
	return func(qd *QueueDefinition) {
		qd.ConsumptionBuckets = buckets
	}
}

// NewQueueDefinition is the QueueDefinition constructor.
func NewQueueDefinition(name QueueName, prefix string,
	consumer MessageConsumer, opts ...QueueDefinitionOption) *QueueDefinition {
	qd := &QueueDefinition{
		Name:               name,
		consumer:           consumer,
		CanonicalName:      queueCanonicalName(prefix, name),
		ConsumptionBuckets: DefBuckets,
	}
	qd.applyOptions(opts...)
	return qd
}

// applyOptions will apply eachj option to the QueueDefinition.
func (qd *QueueDefinition) applyOptions(opts ...QueueDefinitionOption) {
	for _, opt := range opts {
		opt(qd)
	}
}

// queueCanonicalName is the full name, shortnum with prefix as its known by
// the broker.
func queueCanonicalName(prefix string, name QueueName) string {
	return fmt.Sprintf("%s_%s", prefix, name)
}

// DLQCanonicalName is the full name, shortnum with prefix as its known by the
// broker.
func DLQCanonicalName(prefix string, name QueueName) string {
	return fmt.Sprintf("%s_dlq", queueCanonicalName(prefix, name))
}

// ReceiveMessage is the abstraction of the message that sits on the queue.
type ReceiveMessage interface {
	// Deletes the from the queue. This *must be done* or the
	// the message will reappear on the queue.
	Ack() error
	// Payload returns the message as it was received into v
	Unmarshal(v interface{}) error
	// Returns the unique identifier representing the message.
	ID() string
	// Sets the number of seconds this message should be invisible to other consumers
	// This overrides the default visibility set on the queue
	SetVisibilityTimeout(n int) error
}

// SendMessage is the abstraction of the message that is provided to the
// broker.
type SendMessage interface {
	// Marshal serializes the payload for transport to the message queue.
	Marshal() ([]byte, error)
}

// Queue is a simplified interface for acting on a message queue.
type Queue interface {
	SendOne(SendMessage) error
	// SendMany sends multiple message to the queue.
	SendMany([]SendMessage) error
	// Receive a message from the queue. Don't forget to call the Message's
	// Ack() method once the message has been handled!
	ReceiveOne() (ReceiveMessage, error)
	// Fetch metadata by name from broker.
	FetchAttributes([]string) (map[string]string, error)
	// Remove this queue from the broker. (For testing CreateQueue)
	Delete() error
	// Pretty print the name of the queue
	String() string
	// MoveMessages moves the contents of one queue to another. It returns
	// the number of messages moved.
	MoveMessages(Queue) (int, error)
}

// MessageBroker is an interface used to interact with a message broker.
type MessageBroker interface {
	// CreateQueue creates the specified queue if it does not exist or
	// returns the existing queue with the name specified in the
	// QueueDefinition. If a dead letter queue has been specified for
	// the queue, it will be returned as the second queue in returned
	// queue slice.
	CreateQueue(qd *QueueDefinition) ([]Queue, error)
}

// MessageJSON  is a SendMessage implementation for JSON messages
type MessageJSON struct {
	V interface{}
}

// Marshal implements the SendMessage implementation and converts
// the message payload to json or returns an error.
func (m *MessageJSON) Marshal() ([]byte, error) {
	return json.Marshal(m.V)
}

type OnQueueCreation func(qm *QueueManager, q Queue, name QueueName)

// QueueManager (queue manager) simplifies queue access and queue creation.
type QueueManager struct {
	queues          map[QueueName]Queue
	defMap          map[Queue]*QueueDefinition
	qDefs           []*QueueDefinition
	broker          MessageBroker
	onQueueCreation OnQueueCreation
	// If specified the defaultConsumer is used when a consumer has not
	// been specified in the queuedef.
	defaultConsumer MessageConsumer

	metricNamespace     string
	msgInflight         *prometheus.GaugeVec
	msgOutstanding      *prometheus.GaugeVec
	msgSendFailures     *prometheus.CounterVec
	msgSendSuccesses    *prometheus.CounterVec
	msgConsumeSuccesses *prometheus.CounterVec
	msgConsumeFailures  *prometheus.CounterVec
	// Histograms must be created per queue as their bucket configuration
	// may be different.
	msgConsumptionTime map[Queue]prometheus.Histogram
}

// QueueDefinitionOption is a functional option that can modify a QueueManager.
type QueueManagerOption func(*QueueManager)

// NewQueueManager is the QueueManager constructor.
func NewQueueManager(qDefs []*QueueDefinition, mb MessageBroker, opts ...QueueManagerOption) *QueueManager {
	qm := &QueueManager{
		broker:             mb,
		qDefs:              qDefs,
		queues:             make(map[QueueName]Queue, len(qDefs)),
		defMap:             make(map[Queue]*QueueDefinition, len(qDefs)),
		msgConsumptionTime: make(map[Queue]prometheus.Histogram, len(qDefs)),
	}
	for _, opt := range opts {
		opt(qm)
	}
	qm.initMetrics()
	return qm
}

// WithOnQueueCreationCallback adds a callback to be called when queues
// are created.
func WithOnQueueCreationCallback(cb OnQueueCreation) QueueManagerOption {
	return func(qm *QueueManager) {
		qm.onQueueCreation = cb
	}
}

// WithDefaultConsumer specifies a message consumer to use if one was not specified
// in the QueueDefinition.
func WithDefaultConsumer(consumer MessageConsumer) QueueManagerOption {
	return func(qm *QueueManager) {
		qm.defaultConsumer = consumer
	}
}

func WithMetricNamespace(namespace string) QueueManagerOption {
	return func(qm *QueueManager) {
		qm.metricNamespace = namespace
	}
}

func (qm *QueueManager) getMsgOutstanding(q Queue) prometheus.Gauge {
	return qm.msgOutstanding.With(prometheus.Labels{"name": q.String()})
}

func (qm *QueueManager) getMsgSendFailure(q Queue) prometheus.Counter {
	return qm.msgSendFailures.With(prometheus.Labels{"name": q.String()})
}

func (qm *QueueManager) getMsgSendSuccess(q Queue) prometheus.Counter {
	return qm.msgSendSuccesses.With(prometheus.Labels{"name": q.String()})
}

func (qm *QueueManager) getMsgConsumeSuccess(q Queue) prometheus.Counter {
	return qm.msgConsumeSuccesses.With(prometheus.Labels{"name": q.String()})
}

func (qm *QueueManager) getMsgConsumeFailure(q Queue) prometheus.Counter {
	return qm.msgConsumeFailures.With(prometheus.Labels{"name": q.String()})
}

func (qm *QueueManager) getMsgInflight(q Queue) prometheus.Gauge {
	return qm.msgInflight.With(prometheus.Labels{"name": q.String()})
}

func (qm *QueueManager) getMsgConsumptionTime(q Queue) prometheus.Observer {
	return qm.msgConsumptionTime[q]
}

// SendOne sends a message to the specified queue.
func (qm *QueueManager) SendOne(queueName QueueName, msg SendMessage) error {
	q := qm.Get(queueName)
	err := q.SendOne(msg)
	if err != nil {
		qm.getMsgSendFailure(q).Inc()
	} else {
		qm.getMsgSendSuccess(q).Inc()
		qm.getMsgOutstanding(q).Inc()
	}
	return err
}

func (qm *QueueManager) SendMany(queueName QueueName, msgs []SendMessage) error {
	q := qm.Get(queueName)
	err := q.SendMany(msgs)
	if err != nil {
		qm.getMsgSendFailure(q).Inc()
	} else {
		qm.getMsgSendSuccess(q).Inc()
		qm.getMsgOutstanding(q).Add(float64(len(msgs)))
	}
	return err
}

// GetQDef returns the QueueDefintions of the named queue or dies trying. This
// does not require that the queue be created first.
func (qm *QueueManager) GetQDef(name QueueName) *QueueDefinition {
	for _, qdef := range qm.qDefs {
		if qdef.Name == name {
			return qdef
		}
		if qdef.DLQ.Name == name {
			return qdef.DLQ
		}
	}

	log.WithFields(log.Fields{
		"queue": name,
	}).Fatal("QueueDefinition not found for queue")

	return nil
}

// GetDLQName returns the QueueName of the dead-letter queue configured
// for the specified queue.
func (qm *QueueManager) GetDLQName(name QueueName) QueueName {
	def := qm.GetQDef(name)
	if def.DLQ == nil {
		log.WithFields(log.Fields{
			"queue": name,
		}).Fatal("no dead-letter queue configured for queue")
	}
	return def.DLQ.Name
}

func (qm *QueueManager) initMetrics() {
	qm.msgOutstanding = promauto.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: qm.metricNamespace,
			Subsystem: "broker",
			Name:      "msgs",
			Help:      "Number of messages in the queue",
		},
		[]string{"name"},
	)
	qm.msgSendFailures = promauto.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: qm.metricNamespace,
			Subsystem: "broker",
			Name:      "send_fail_total",
			Help:      "Number of failed message sends to queue",
		},
		[]string{"name"},
	)
	qm.msgSendSuccesses = promauto.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: qm.metricNamespace,
			Subsystem: "broker",
			Name:      "send_success_total",
			Help:      "Number of successful message sends",
		},
		[]string{"name"},
	)
	qm.msgConsumeFailures = promauto.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: qm.metricNamespace,
			Subsystem: "broker",
			Name:      "consume_fail_total",
			Help:      "Number of messages that failed to be consumed",
		},
		[]string{"name"},
	)
	qm.msgConsumeSuccesses = promauto.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: qm.metricNamespace,
			Subsystem: "broker",
			Name:      "consume_success_total",
			Help:      "Number of messages consumed",
		},
		[]string{"name"},
	)
	qm.msgInflight = promauto.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: qm.metricNamespace,
			Subsystem: "broker",
			Name:      "inflight_total",
			Help:      "Number of messages being consumed",
		},
		[]string{"name"},
	)
}

func (qm *QueueManager) addQueue(queue Queue, name QueueName, qDef *QueueDefinition) {
	qm.queues[name] = queue
	qm.defMap[queue] = qDef
	if qm.onQueueCreation != nil {
		qm.onQueueCreation(qm, queue, name)
	}
	qm.msgConsumptionTime[queue] = promauto.NewHistogram(
		prometheus.HistogramOpts{
			Namespace: qm.metricNamespace,
			Subsystem: "broker",
			Name:      "consumption_time_seconds",
			Buckets:   qDef.ConsumptionBuckets,
			ConstLabels: map[string]string{
				"name": queue.String(),
			},
		},
	)
}

// Get returns the requested queue or dies trying.
func (qm *QueueManager) Get(name QueueName) Queue {
	q, ok := qm.queues[name]
	if !ok {
		qdef := qm.GetQDef(name)
		queues, err := qm.broker.CreateQueue(qdef)
		if err != nil {
			log.WithFields(log.Fields{
				"err":   err,
				"queue": name,
			}).Fatal("failed to create queue")
		}
		q = queues[0]
		qm.addQueue(q, name, qdef)
		// queue has a dead-letter queue configured
		if len(queues) > 1 {
			qm.addQueue(queues[1], qdef.DLQ.Name, qdef.DLQ)
		}
	}
	return q
}

// Delete removes the named queue from the broker and removes its
// the queue from its cache.
func (qm *QueueManager) Delete(name QueueName) error {
	q, ok := qm.queues[name]
	if !ok {
		return nil
	}

	if collector, ok := qm.msgConsumptionTime[q]; ok {
		prometheus.DefaultRegisterer.Unregister(collector)
		delete(qm.msgConsumptionTime, q)
	}

	err := q.Delete()
	if err != nil {
		return err
	}
	delete(qm.queues, name)
	return nil
}

// GetByNameString returns the Queue using the string name of the Queue. Will
// halt program execution if an invalid queue is given (a queue without a
// QueueDefinition)
func (qm *QueueManager) GetByNameString(name string) Queue {
	qName := qm.QueueName(strings.ToLower(name))
	return qm.Get(qName)
}

// QueueName returns the QueueName that matches the string or dies trying.
func (qm *QueueManager) QueueName(name string) QueueName {
	for _, qd := range qm.qDefs {
		if string(qd.Name) == name {
			return qd.Name
		}
	}

	panic(fmt.Sprintf("no such queue `%s`", name))
}

func truncate(s string, length int) string {
	if length > len(s) {
		return s
	}
	return s[0:30]
}

func (qm *QueueManager) consume(q Queue, msg ReceiveMessage, consumer MessageConsumer) {
	qm.getMsgInflight(q).Inc()
	timer := prometheus.NewTimer(prometheus.ObserverFunc(func(v float64) {
		qm.getMsgConsumptionTime(q).Observe(v)
	}))
	defer timer.ObserveDuration()
	defer qm.getMsgInflight(q).Dec()

	log.WithFields(log.Fields{
		"msgID": truncate(msg.ID(), 30),
		"queue": q.String(),
	}).Info("processing message")

	success := consumer.Consume(qm, q, msg)
	if success {
		log.WithFields(log.Fields{
			"msgID": truncate(msg.ID(), 30),
			"queue": q.String(),
		}).Info("message consumed")
		msg.Ack()

		qm.getMsgConsumeSuccess(q).Inc()
		qm.getMsgOutstanding(q).Dec()
	} else {
		log.WithFields(log.Fields{
			"msgID": truncate(msg.ID(), 30),
			"queue": q.String(),
		}).Info("message consumption failed")
		qm.getMsgConsumeFailure(q).Inc()
		// On failure we want to make message visible again so that
		// it can either a) be retried again if Retry Count > 1, or
		// moved to the DLQ
		msg.SetVisibilityTimeout(0)
	}
}

func (qm *QueueManager) process(q Queue, msgCh <-chan ReceiveMessage,
	done chan<- bool, ready chan<- bool) {
	log.WithFields(log.Fields{
		"queue": q.String(),
	}).Trace("processor started")

	consumer := qm.defMap[q].consumer
	if consumer == nil {
		consumer = qm.defaultConsumer
	}

	ready <- true
	for msg := range msgCh {
		qm.consume(q, msg, consumer)
		ready <- true
	}

	log.WithFields(log.Fields{
		"queue": q.String(),
	}).Info("processor stopped")

	done <- true
}

func shutdown(doneCh <-chan bool, opts *QueueProcessingOptions) {
	for i := 0; i < opts.Parallelism; i++ {
		<-doneCh
	}
	log.Info("processors have exited cleanly")
	opts.Done <- true
}

func (qm *QueueManager) processN(q Queue, opts *QueueProcessingOptions) {
	log.WithFields(log.Fields{
		"processors": opts.Parallelism,
	}).Info("starting queue processing go routines")

	msgCh := make(chan ReceiveMessage)
	doneCh := make(chan bool)
	readyCh := make(chan bool, opts.Parallelism)
	for i := 0; i < opts.Parallelism; i++ {
		go qm.process(q, msgCh, doneCh, readyCh)
	}

	// wait until a processor signals that its ready to retrieve
	// a message from the queue before fetching it.
	for range readyCh {
		// check if we should exit before receiving another message
		select {
		case sig := <-opts.Signals:
			log.WithFields(log.Fields{
				"signal": sig,
				"queue":  q.String(),
			}).Info("received signal")
			close(msgCh)

			log.Debug("waiting for quitting messages")
			shutdown(doneCh, opts)
			return
		default:
		}

		for {
			msg, err := q.ReceiveOne()
			if err != nil {
				switch err {
				case ErrQueueNoMessages:
					log.WithFields(log.Fields{
						"queue": q.String(),
					}).Info("no messages")
				case ErrQueuePollError:
					log.WithFields(log.Fields{
						"queue": q.String(),
						"err":   err,
					}).Warn("got poll error, sleeping and retrying")
					time.Sleep(5 * time.Second)
				default:
					log.WithFields(log.Fields{
						"queue": q.String(),
						"err":   err,
					}).Error("error retrieving messages from broker")
					shutdown(doneCh, opts)
					return
				}
			} else {
				msgCh <- msg
				if opts.OneMsgOnly {
					close(msgCh)
					shutdown(doneCh, opts)
					return

				}
				break
			}

			select {
			case sig := <-opts.Signals:
				log.WithFields(log.Fields{
					"signal": sig,
					"queue":  q.String(),
				}).Info("received signal")
				close(msgCh)

				log.Debug("waiting for quitting messages")
				shutdown(doneCh, opts)
				return
			default:
			}
		}
	}
}

// QueueProcessingOptions controls how the queue is processed.
type QueueProcessingOptions struct {
	// amount of parallelism desired (number of consumer go routines)
	Parallelism int
	// signals passed from controller to QueueManager ('quit')
	Signals chan string
	// signals that processing has been stopped and the controller can exit
	Done chan bool
	// quits after processing only one message. Testing only!
	OneMsgOnly bool
}

// NewQueueProcessingOptions constructs queue processing options so we do not
// have to duplicate it.
func NewQueueProcessingOptions(parallelism int) *QueueProcessingOptions {
	return &QueueProcessingOptions{
		Parallelism: parallelism,
		Signals:     make(chan string, 1),
		Done:        make(chan bool, 1),
	}
}

// Process receives messages from the the message broker and passes each one to
// the message consumer defined for each queue in its QueueDefinition. Thispongs chan<- string
// method will run until the StopReceive method is called on the queue.
func (qm *QueueManager) Process(q Queue, opts *QueueProcessingOptions) {
	qm.processN(q, opts)
}

// ProcessOne fetches one message residing in the queue, processes it and
// returns. It's meant for use in tests. Simulates how it's used in tests.
func (qm *QueueManager) ProcessOne(q Queue) {
	// polling done in a go routine and one consumer running in a different
	// go routine.
	opts := NewQueueProcessingOptions(1)
	opts.OneMsgOnly = true
	go qm.processN(q, opts)
	<-opts.Done
}

// TestQueuesCreate intializes all the queues defined in the queue definitions
// held by the QueueManager.
func (qm *QueueManager) TestQueuesCreate() {
	for _, qDef := range qm.qDefs {
		q := qm.Get(qDef.Name)
		if q == nil {
			log.WithFields(log.Fields{
				"queue": qDef.Name,
			}).Fatal("failed to retrieve queue")
		}
	}
}

// May need to unregister metrics between test runs.
func (qm *QueueManager) unregisterMetrics() {
	for _, collector := range []prometheus.Collector{
		qm.msgInflight,
		qm.msgOutstanding,
		qm.msgSendFailures,
		qm.msgSendSuccesses,
		qm.msgConsumeSuccesses,
		qm.msgConsumeFailures,
	} {
		prometheus.DefaultRegisterer.Unregister(collector)
	}
}

// TestQueuesDestroy removes the state of the queues from the broker so that
// new queues can be created/initialized.
func (qm *QueueManager) TestQueuesDestroy() {
	qm.unregisterMetrics()
	for _, q := range qm.queues {
		q.Delete()
	}
}
