# Broker

Offers an interface over basic message queue options. Broker supports AWS SQS
and a dead-simple, slice-based in-memory broker.

## broker.go

This section summarizes the important types defined in `broker.go`.

### type QueueDefinition

`QueueDefinition` houses configuration needed to create a queue. It is
agnostic to underlying provider, but should contain all of the information
needed to uniquely identify a queue on any provider.

### Queue interface

`Queue` provides us with a way to communicate with a message queue.

### MessageBroker interface

`MessageBroker` is an interface used to communicate with a message broker. It
makes the call to create a queue given a `QueueDefinition`.

### QueueManager

`QueueManager` is a struct that makes managing queues easier. It contains a
mix of things - for example, it contains `QueueDefinition`s, but also contains
the queues themselves. It also contains prometheus gauges and callbacks. As
such, there should only be one instance of `QueueManager` per client.
