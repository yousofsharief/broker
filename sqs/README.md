# Experimenting with SQS queues on localhost

This repository doesn't have any tests, but it should. In the meantime, it's
simple to get up and running with SQS locally using [localstack](https://github.com/localstack/localstack).

First run the docker-compose.yml file that can be found in this directory:

```
docker-compose up
```

Then, in a file somewhere create a new SQS instance (implements MessageBroker):

```
config := aws.NewConfig().WithEndpoint("http://localhost:4566")
sqs := sqs.New(awsconfig, <TIMEOUT>)
```

If these lines run without panicing (.Must will panic if session is invalid),
then you're ready to start experimenting!
